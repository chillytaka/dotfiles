# DOTFILES by BLANK

## PREQUISITES

- i3-rounded-gaps (aur)
- feh
- picom
- rofi
- polybar (aur)
- python-dbus
- python-gobject

## FONTS

- adobe-source-code-pro-fonts
- nerd-fonts-source-code-pro

## installation

1. First install all of the above packages using pacman and yay. If you are using another distro (for example, Ubuntu) use your own package manager.

   ```
   sudo pacman -Syy
   sudo pacman -S feh picom rofi python-dbus python-gobject adobe-source-code-pro-fonts nerd-fonts-source-code-pro
   ```

   ```
   yay -S i3-rounded-gaps polybar
   ```

2. run `i3-config-wizard` , this command will initialize the default i3 configuration.

3. Copy the entire directory on to `/home/[USER]/.config/`, make sure you change the [USER] to your own username.

## SHORTCUT

### Main Shortcut

there are a lot of shortcut available on the `I3` and you can even adding more by yourself, this is some default keybindings with VIM as a reference.

all of this shortcut is used with `Mod` key set up as `Super`

| Command                         |       Shortcut        |           Alternative |
| ------------------------------- | :-------------------: | --------------------: |
| Default Mod                     |        `Super`        |                       |
| Kill Program                    |   `Mod + Shift + w`   |                       |
| Open Rofi                       |       `Mod + d`       |                       |
| Move Focus Left                 |       `Mod + h`       |          `Mod + Left` |
| Move Focus Right                |       `Mod + l`       |         `Mod + Right` |
| Move Focus Up                   |       `Mod + k`       |            `Mod + Up` |
| Move Focus Down                 |       `Mod + j`       |          `Mod + Down` |
| Move Focused Window Left        |   `Mod + Shift + h`   |  `Mod + Shift + Left` |
| Move Focused Window Right       |   `Mod + Shift + l`   | `Mod + Shift + Right` |
| Move Focused Window Up          |   `Mod + Shift + k`   |    `Mod + Shift + Up` |
| Move Focused Window Down        |   `Mod + Shift + j`   |  `Mod + Shift + Down` |
| Split Horizontal                |   `Mod + Shift + v`   |                       |
| Split Vertical                  |       `Mod + v`       |                       |
| FullScreen Toggle               |       `Mod + f`       |                       |
| Change Layout to Stacking       |       `Mod + s`       |                       |
| Change Layout to Tabbed         |       `Mod + w`       |                       |
| Change Layout to Tiling         |       `Mod + e`       |                       |
| Toggle Floating and Tiling Mode | `Mod + Shift + Space` |                       |

### Workspaces

`I3` have a concept of workspace, if you feel confused about workspace, just think of it as a virtual desktop. You could move from one workspace to another using `Mod + Workspace Number` and move one window to another using `Mod + Shift + Workspace Number`

| Command                        |       Shortcut        | Alternative |
| ------------------------------ | :-------------------: | ----------: |
| Move to Workspace [0-9]        |     `Mod + [0-9]`     |             |
| Move Window to Workspace [0-9] | `Mod + Shift + [0-9]` |             |

### Restart / Reload I3

there might be some cases where you need to restart `I3` but dont want to logout when doing so, this is especially useful when you are trying a new I3 configuration.

| Command                    |     Shortcut      | Alternative |
| -------------------------- | :---------------: | ----------: |
| Reload Configuration Files | `Mod + Shift + C` |             |
| Reload I3                  | `Mod + Shift + R` |             |

### Resize Window

sometimes, you feel that the window is too small or maybe too big, this is particularly useful when you want your window not to eat a lot of screen space

| Command             | Shortcut  | Alternative |
| ------------------- | :-------: | ----------: |
| Enter Resize Mode   | `Mod + R` |             |
| Quit Resize Mode    |   `Esc`   |             |
| Shrink Width 10 px  | `Mod + h` |      `Left` |
| Grow Width 10 px    | `Mod + l` |     `Right` |
| Shrink Height 10 px | `Mod + k` |        `Up` |
| Grow Height 10 px   | `Mod + j` |      `Down` |

### Remapping CapsLock

if you are using VIM, you will need to use a lot of `Esc` Key, that's why some people choose to remap their nearly-useless `Caps-Lock` Key to `Esc` Key. I have already set this as the default behaviour but feel free to not use this by commenting this line:

file location : `/home/[USER]/.config/i3/config`

```
exec_always xmodmap -e "clear lock"
exec_always xmodmap -e "keysym Caps_Lock = Escape"
```

## Feh Wallpeper Changer

my configuration run feh as the default wallpaper changer, the default folder is at `$HOME/Pictures/WP`.
if you don't want to use that directory, change this line to whatever directory that you desire :

file location : `/home/[USER]/.config/i3/config`

```
exec --no-startup-id feh --randomize --bg-fill $HOME/Pictures/WP
```

note: unfortunately, if you want the wallpaper to change periodically, Feh doesn't supported that kind of behaviour. You could, however, set the command as a crontask.

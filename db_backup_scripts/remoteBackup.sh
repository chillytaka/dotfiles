#!/bin/bash

DIR=$(dirname -- "$0")
source "$DIR/util.sh"

#SOURCE_DIRECTORY=/home/backup/database
#TARGET_DIRECTORY=/home/backup/uploadTarget
SOURCE_DIRECTORY=/home/aufanabil/test
TARGET_DIRECTORY=/home/aufanabil/backup
RCLONE_TARGET=pcloud-main
RCLONE_DIRECTORY=database/backup
LOG_FOLDER=$DIR/logs

# for logging, we convert the date format to use weekday name
# so the logging will automatically rewrite itself per-week
DATE_LOG_FILE=$(date -d "$DATE" "+%A")
LOG_FILE="$LOG_FOLDER/$DATE_LOG_FILE.log"
initLog "$LOG_FILE"

doLog "creating target directory"
mkdir --parent "$TARGET_DIRECTORY"

doLog "copy backed-up db data"
for item in "$SOURCE_DIRECTORY"/*; do
  # copy the latest file to target directory for easier uploading
  doLog "handling $item"

  # Reza backup directory has a different backup script, hence different handling
  if [[ $item == "$SOURCE_DIRECTORY/Reza" ]]; then
    # shellcheck disable=SC2012
    latestFiles=$(ls -Art1 "$item" | tail -n 7)

    for file in $latestFiles; do
      doLog "copy $file"
      cp "$item/$file" $TARGET_DIRECTORY
    done
    continue
  fi

  # shellcheck disable=SC2012
  latestFile=$(ls -Art1 "$item" | tail -n 1)

  if [[ $latestFile == "" ]]; then
    continue
  fi

  doLog "copy $latestFile"
  cp "$item/$latestFile" $TARGET_DIRECTORY
done

doLog "uploading to remote"
currentDate=$(date -d "yesterday" +"%Y/%m/%d")
rcloneTargetPath="$RCLONE_TARGET://$RCLONE_DIRECTORY/$currentDate"
rclone copy --checksum --transfers=10 -L --stats 2h --stats-log-level NOTICE "$TARGET_DIRECTORY" "$rcloneTargetPath" >>"${LOG_FILE}" 2>&1

doLog "delete old data in remote"
# for space saving, we only store up-to 14 previous days in remote
deleteDate=$(date -d "14 days ago" +"%Y/%m/%d")
rcloneDeleteTargetPath="$RCLONE_TARGET://$RCLONE_DIRECTORY/$deleteDate"
rclone purge "$rcloneDeleteTargetPath" >>"${LOG_FILE}" 2>&1

doLog "cleanup"
rm -rf $TARGET_DIRECTORY

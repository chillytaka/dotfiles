# zsh history config
HISTFILE=~/.zsh_history
HISTSIZE=1000
SAVEHIST=1000

### PATH ###
export PATH="$HOME/Master/arcanist/bin:$PATH"
export PATH="$HOME/.local/bin:$PATH"
export PATH="$HOME/.bcl/cli:$PATH"
export PATH="$HOME/Project/docker_config:$PATH"
### End of PATH ###

### CUSTOM ENV. VARIABLE ###
export GPG_TTY=$(tty)

# NVM config
export NVM_LAZY_LOAD=true

# PKICTL
export PKICTL_MEMBER_CONTEXT=aufa.amiri@cermati.com@10-6f-d9-f7-ef-ef@pkictl-prod
export PKICTL_MEMBER_CERT_FILE=/home/aufanabil/.certs/certs/member/aufa.amiri@cermati.com@10-6f-d9-f7-ef-ef@pkictl-prod/aufa.amiri@cermati.com@10-6f-d9-f7-ef-ef@pkictl-prod.crt
export PKICTL_MEMBER_KEY_FILE=/home/aufanabil/.certs/certs/member/aufa.amiri@cermati.com@10-6f-d9-f7-ef-ef@pkictl-prod/.private/aufa.amiri@cermati.com@10-6f-d9-f7-ef-ef@pkictl-prod.key
export PKICTL_MEMBER_CA_CERT_FILE=/home/aufanabil/.certs/certs/member/aufa.amiri@cermati.com@10-6f-d9-f7-ef-ef@pkictl-prod/aufa.amiri@cermati.com@10-6f-d9-f7-ef-ef@pkictl-prod.cert-ca-chain.crt
export PKICTL_MEMBER_ROOT_CA_CERT_FILE=/home/aufanabil/.certs/certs/member/aufa.amiri@cermati.com@10-6f-d9-f7-ef-ef@pkictl-prod/aufa.amiri@cermati.com@10-6f-d9-f7-ef-ef@pkictl-prod.root-ca.crt
export PKICTL_MEMBER_SIGNER_CA_CERT_FILE=/home/aufanabil/.certs/certs/member/aufa.amiri@cermati.com@10-6f-d9-f7-ef-ef@pkictl-prod/aufa.amiri@cermati.com@10-6f-d9-f7-ef-ef@pkictl-prod.intermediate-ca.crt
export PKICTL_MEMBER_PKCS12_FILE=/home/aufanabil/browser-certificates/aufa.amiri@cermati.com@10-6f-d9-f7-ef-ef@pkictl-prod.p12
export PKICTL_USERNAME=aufa.amiri@cermati.com
export ARCANIST_TLS_CERT=~/.certs/certs/member/aufa.amiri@cermati.com@10-6f-d9-f7-ef-ef@pkictl-prod/aufa.amiri@cermati.com@10-6f-d9-f7-ef-ef@pkictl-prod.crt
export ARCANIST_TLS_KEY=~/.certs/certs/member/aufa.amiri@cermati.com@10-6f-d9-f7-ef-ef@pkictl-prod/.private/aufa.amiri@cermati.com@10-6f-d9-f7-ef-ef@pkictl-prod.key
export PKI_CERT=~/.certs/certs/member/aufa.amiri@cermati.com@10-6f-d9-f7-ef-ef@pkictl-prod/aufa.amiri@cermati.com@10-6f-d9-f7-ef-ef@pkictl-prod.crt
export PKI_KEY=~/.certs/certs/member/aufa.amiri@cermati.com@10-6f-d9-f7-ef-ef@pkictl-prod/.private/aufa.amiri@cermati.com@10-6f-d9-f7-ef-ef@pkictl-prod.key
export PKI_CA=~/.certs/certs/member/aufa.amiri@cermati.com@10-6f-d9-f7-ef-ef@pkictl-prod/aufa.amiri@cermati.com@10-6f-d9-f7-ef-ef@pkictl-prod.root-ca.crt
export PKICTL_CERT_PEM_FILE=~/.certs/certs/member/aufa.amiri@cermati.com@10-6f-d9-f7-ef-ef@pkictl-prod/aufa.amiri@cermati.com@10-6f-d9-f7-ef-ef@pkictl-prod.pem

### End of CUSTOM ENV. VARIABLE ###

### pure theme config ###
PURE_CMD_MAX_EXEC_TIME=60
zstyle :prompt:pure:git:stash show yes
### end of pure theme config ###

### Custom Functions ###
rajamoney_init () {
  cd ~/Project/rajamoney/
  cli/kloudctl login aws cermati-cermati-rajamoney developer
  source ~/.kloudctl/.login/aws.281457462017.cermati-cermati-rajamoney-developer-iu
  export S3_KEY_READ=$AWS_ACCESS_KEY_ID
  export S3_SECRET_READ=$AWS_SECRET_ACCESS_KEY
  export S3_KEY=$AWS_ACCESS_KEY_ID
  export S3_SECRET=$AWS_SECRET_ACCESS_KEY
  export AWS_S3_STS_MASTER_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
  export AWS_S3_STS_MASTER_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY
  cli/kloudctl login gcp cermati-cermati-rajamoney-stg1 developer
  cp ~/.kloudctl/.login/gcp.amiable-crane-89702.cermati-cermati-rajamoney-stg1-developer-sa.json google_service_account_credentials.json
}

payment_init () {
  cd ~/Project/payment
  cli/kloudctl login gcp cermati-cermati-payment-stg1 developer
  cp ~/.kloudctl/.login/gcp.amiable-crane-89702.cermati-cermati-payment-stg1-developer-sa.json google_pub_sub_credentials.json
}

insurancemerchant_init () {
  cd ~/Project/merlin/
  cli/kloudctl login gcp cermati-insurancemerchant-stg-jkt developer
}

recurring_init () {
  cd ~/Project/merlin/
  cli/kloudctl login gcp cermati-batch-stg1 developer
}

updateAll () {
  sudo apt update
  sudo apt upgrade -y
}

prj () {
  if [[ $2 = 'start' ]]; then

    if [[ $(command -v $1_init) ]]; then
      $1_init
    fi

    cd ~/Project/$1 && npm run start

  else
    cd ~/Project/$1 && $@[2,-1]
  fi
}

# auto completion for prj func
_prj () {
  _arguments '1:filename:->directory' '2:actions:(npm start)'
  case $state in
    directory)
      _files -W ~/Project
    ;;
  esac
}
compdef _prj prj
### End of Custom Functions ###

autoload -Uz compinit bashcompinit

### Added by Zinit's installer
if [[ ! -f $HOME/.local/share/zinit/zinit.git/zinit.zsh ]]; then
    print -P "%F{33} %F{220}Installing %F{33}ZDHARMA-CONTINUUM%F{220} Initiative Plugin Manager (%F{33}zdharma-continuum/zinit%F{220})…%f"
    command mkdir -p "$HOME/.local/share/zinit" && command chmod g-rwX "$HOME/.local/share/zinit"
    command git clone https://github.com/zdharma-continuum/zinit "$HOME/.local/share/zinit/zinit.git" && \
        print -P "%F{33} %F{34}Installation successful.%f%b" || \
        print -P "%F{160} The clone has failed.%f%b"
fi

source "$HOME/.local/share/zinit/zinit.git/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit

# Load a few important annexes, without Turbo
# (this is currently required for annexes)
zinit light-mode for \
    zdharma-continuum/zinit-annex-as-monitor \
    zdharma-continuum/zinit-annex-bin-gem-node \
    zdharma-continuum/zinit-annex-patch-dl \
    zdharma-continuum/zinit-annex-rust

### End of Zinit's installer chunk
zinit ice wait lucid atload'_zsh_autosuggest_start'
zinit light zsh-users/zsh-autosuggestions

###  Plugins Config ###
zinit wait lucid light-mode for \
  zsh-users/zsh-syntax-highlighting \
  lukechilds/zsh-nvm
### End of Plugins Config ###

### Load Theme ###
zinit ice compile'(pure|async).zsh' pick'async.zsh' src'pure.zsh'
zinit light sindresorhus/pure
### End of Load Theme ###

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/home/aufanabil/.sdkman"
[[ -s "/home/aufanabil/.sdkman/bin/sdkman-init.sh" ]] && source "/home/aufanabil/.sdkman/bin/sdkman-init.sh"
